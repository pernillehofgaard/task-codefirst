﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using Task11_CodeFirst.Models;

namespace Task12_CodeFirst
{
	class PostGrad : DbContext
	{
		public DbSet<Student> Student { get; set; }
		public DbSet<Professor> Professor { get; set; }
		public DbSet<ProfessorQualification> ProfessorQualification { get; set; }
		public DbSet<Qualification> Qualifications { get; set; }
		public DbSet<ResearchProject> ResearchProject { get; set; }
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
		{
			optionBuilder.UseSqlServer("Data Source=PC7370\\SQLEXPRESS;Initial Catalog=PostGradCodeFirstDB; Integrated Security=True;");
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//ProfessorId & QualificationId is PK in linking table
			modelBuilder.Entity<ProfessorQualification>().HasKey(professorQualification => new { professorQualification.ProfessorId, professorQualification.QualificationId });
			
			//seeding data to Qualification table
			modelBuilder.Entity<Qualification>().HasData(new Qualification { Id = 1, AwardedDate = new DateTime(1997, 02, 20), Category = "Biology", Name = "Bachelor" });
			modelBuilder.Entity<Qualification>().HasData(new Qualification { Id = 2, AwardedDate = new DateTime(2004, 12, 24), Category = "Math", Name = "PhD" });
			modelBuilder.Entity<Qualification>().HasData(new Qualification { Id = 3, AwardedDate = new DateTime(1994, 09, 01), Category = "Cooking", Name = "Masters" });
			modelBuilder.Entity<Qualification>().HasData(new Qualification { Id = 4, AwardedDate = new DateTime(1963, 04, 25), Category = "Coding", Name = "Bachelor" });
			modelBuilder.Entity<Qualification>().HasData(new Qualification { Id = 5, AwardedDate = new DateTime(1969, 01, 31), Category = "UI-design", Name = "Diploma" });

			//seeding data to ProfessorQualification table
			modelBuilder.Entity<ProfessorQualification>().HasData(new ProfessorQualification { ProfessorId = 1, QualificationId = 5 }); 
			modelBuilder.Entity<ProfessorQualification>().HasData(new ProfessorQualification { ProfessorId = 1, QualificationId = 4 }); //Professor1, Kari Nordmann has 2 Qualifications
			modelBuilder.Entity<ProfessorQualification>().HasData(new ProfessorQualification { ProfessorId = 2, QualificationId = 3 });
			modelBuilder.Entity<ProfessorQualification>().HasData(new ProfessorQualification { ProfessorId = 3, QualificationId = 4 });
			modelBuilder.Entity<ProfessorQualification>().HasData(new ProfessorQualification { ProfessorId = 4, QualificationId = 1 });
			modelBuilder.Entity<ProfessorQualification>().HasData(new ProfessorQualification { ProfessorId = 5, QualificationId = 5 });

			//seeding data to Professor table
			modelBuilder.Entity<Professor>().HasData(new Professor { Id = 1, FirstName = "Kari", LastName = "Nordmann", Subject = "Coding"});
			modelBuilder.Entity<Professor>().HasData(new Professor { Id = 2, FirstName = "Ola", LastName = "Nordmann", Subject = "Math" });
			modelBuilder.Entity<Professor>().HasData(new Professor { Id = 3, FirstName = "Nick", LastName = "Lennox", Subject = "Coding"});
			modelBuilder.Entity<Professor>().HasData(new Professor { Id = 4, FirstName = "Bjørn", LastName = "Blikstad", Subject = "Biology"});
			modelBuilder.Entity<Professor>().HasData(new Professor { Id = 5, FirstName = "Dewald", LastName = "Els", Subject = "UI-design"});
		
			//seeding data to student table
			modelBuilder.Entity<Student>().HasData(new Student { Id = 1, FirstName = "Pernille", LastName = "Hofgaard", DOB = new DateTime(1997, 02, 20), ProfessorId = 5 });
			modelBuilder.Entity<Student>().HasData(new Student { Id = 2, FirstName = "Oda", LastName = "Hofgaard", DOB = new DateTime(1997, 02, 20), ProfessorId = 4 });
			modelBuilder.Entity<Student>().HasData(new Student { Id = 3, FirstName = "Emil", LastName = "Kalstø", DOB = new DateTime(1994, 09, 01), ProfessorId = 3 });
			modelBuilder.Entity<Student>().HasData(new Student { Id = 4, FirstName = "Marte", LastName = "Brekke", DOB = new DateTime(1985, 01, 01), ProfessorId = 2 });
			modelBuilder.Entity<Student>().HasData(new Student { Id = 5, FirstName = "Jon", LastName = "Otessen", DOB = new DateTime(1997, 10, 28), ProfessorId = 1 });

			//seeding data to ResearchProject table
			modelBuilder.Entity<ResearchProject>().HasData(new ResearchProject { Id = 1, StartDate = new DateTime(2020, 02, 01), Title = "Corona Vaksine", StudentId = 5 });
			modelBuilder.Entity<ResearchProject>().HasData(new ResearchProject { Id = 2, StartDate = new DateTime(1986, 04, 26), Title = "Radiation testing", StudentId = 4});
			modelBuilder.Entity<ResearchProject>().HasData(new ResearchProject { Id = 3, StartDate = new DateTime(2000, 01, 01), Title = "Baby boom experiment", StudentId = 3});
			modelBuilder.Entity<ResearchProject>().HasData(new ResearchProject { Id = 4, StartDate = new DateTime(1940, 01, 01), Title = "Twin experiment", StudentId = 2});
			modelBuilder.Entity<ResearchProject>().HasData(new ResearchProject { Id = 5, StartDate = new DateTime(2012, 01, 01), Title = "End of the world", StudentId = 1 });
			
        }
	}
}
