﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Task11_CodeFirst.Models;

namespace Task12_CodeFirst
{
    class Professor
    {
        public int Id { get; set; } //PK
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Subject { get; set; }

        //Collections of students and Qualifications
        public ICollection<Student> Students { get; set; }
        public ICollection<ProfessorQualification> professorQualifications { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
