﻿using System;
using System.Collections.Generic;
using System.Text;
using Task12_CodeFirst;

namespace Task11_CodeFirst.Models
{
	class ResearchProject
	{
        public int Id { get; set; } //PK
        public string Title { get; set; }
        public DateTime StartDate { get; set; }

        //Nav prop
        public Student Student { get; set; }
        public int StudentId { get; set; } // FK
        
    }
}
