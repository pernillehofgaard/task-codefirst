﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace Task12_CodeFirst
{
	class Program
	{
		static void Main(string[] args)
		{
			writeToJson();

			bool keepGoing = true;
			while (keepGoing)
			{
				Console.WriteLine("What would you like to do: ");
				Console.WriteLine("\n1 - View all students" +
								  "\n2 - View all professors" +
								  "\n3 - Update student" + 
								  "\n4 - Add student to database" +
								  "\n5 - Delete student" +
								  "\n6 - quit");
				int answer = Convert.ToInt32(Console.ReadLine());

				switch (answer)
				{
					case 1:
						Console.WriteLine("All students: ");
						PrintAllStudents();
						break;
					case 2:
						PrintAllProfessors();
						break;
					case 3:
						UpdateStudent();
						break;
					case 4:
						CreateStudentForm();
						break;
					case 5:
                        //Not called because it might mess with the code
                        Console.WriteLine("Not implemented method. See code for info");
						//DeleteStudentFrom();
						break;
					case 6:
						Console.WriteLine("Quiting");
						keepGoing = false;
						break;
					default:
						Console.WriteLine("Not an option");
						break;
				}
			}
		}

		private static void PrintAllStudents()
		{
			using(PostGrad postGrad = new PostGrad())
			{
				List<Student> students = postGrad.Student.ToList();

				foreach (Student studs in students)
				{
					Console.WriteLine(studs.Id + ", " + studs.FirstName + ", " + studs.LastName + ", " + studs.DOB + ", " + studs.ProfessorId);
				}
			}
		}

		//Write objects to json files
		private static void writeToJson()
        {
			using(PostGrad postGrad = new PostGrad())
            {
				//write Professor object to json file
				string jsonProfessor = System.Text.Json.JsonSerializer.Serialize(postGrad.Professor);
				File.WriteAllText("jsonProfessor", jsonProfessor);
			}
		}

		private static void PrintAllProfessors()
		{
			using(PostGrad postGrad = new PostGrad())
			{
				List<Professor> professors = postGrad.Professor.ToList();

				foreach(Professor prof in professors)
				{
					Console.WriteLine(prof.Id + ", " + prof.FirstName + ", " + prof.LastName + ", " + prof.professorQualifications);
				}
			}
		}

		//update lastname or supervisor
		private static void UpdateStudent()
		{
			Console.WriteLine("Which student do you want to change?");
			Console.WriteLine("Id: ");
			int studentIdToChange = Convert.ToInt32(Console.ReadLine());

			using(PostGrad postGrad = new PostGrad())
			{
				Student student = postGrad.Student.Find(studentIdToChange);
				Console.WriteLine(student.FirstName + " " + student.LastName + " " + student.DOB);

				Console.WriteLine("Change Lastname or supervisor?");
				string change = Console.ReadLine().ToUpper();
				
				if (change.Equals("LASTNAME"))
				{
					Console.WriteLine("Change last name to: ");
					string newLastName = Console.ReadLine();
					student.LastName = newLastName;
					postGrad.SaveChanges();
					Console.WriteLine("Updated last name to : " + student.LastName);
					
				}
				else
				{
					//change supervisor
					int currentSupervisor = student.ProfessorId;
					Professor professor = postGrad.Professor.Find(currentSupervisor);
					Console.WriteLine("Current supervisor: " + professor.FirstName + " " + professor.LastName);

					
					//print all professors so user knows Id of professors
					List<Professor> professors = postGrad.Professor.ToList();
					foreach (Professor prof in professors)
					{
						Console.WriteLine("Id: " + prof.Id + ", name: " + prof.FirstName + " " + prof.LastName);
					}

					Console.WriteLine("Change to (Id): ");
					int newSupervisor = Convert.ToInt32(Console.ReadLine());
					student.ProfessorId = newSupervisor;
					postGrad.SaveChanges();
					Console.WriteLine("Supervisor changed to " + postGrad.Professor.Find(newSupervisor).FirstName + " " + postGrad.Professor.Find(newSupervisor).LastName);

				}
				
			}
		}

		private static void CreateStudentForm()
		{
			try
			{
				using (PostGrad postGrad = new PostGrad())
				{
					Console.WriteLine("Enter first name: ");
					string firstName = Console.ReadLine();

					Console.WriteLine("Enter last name: ");
					string lastname = Console.ReadLine();

					Console.WriteLine("Enter year of birt (yyyy): ");
					int yyyy = Convert.ToInt32(Console.ReadLine());

					Console.WriteLine("Enter month of birth (mm): ");
					int mm = Convert.ToInt32(Console.ReadLine());

					Console.WriteLine("Enter day of birth (dd): ");
					int dd = Convert.ToInt32(Console.ReadLine());
					DateTime dob = new DateTime(yyyy, mm, dd);

					List<Professor> professors = postGrad.Professor.ToList();
					foreach (Professor profs in professors)
					{
						Console.WriteLine("Id: " + profs.Id + ", name: " + profs.FirstName + " " + profs.LastName);
					}

					Console.WriteLine("Enter professor id: ");
					int professorId = Convert.ToInt32(Console.ReadLine());

					CreateNewStudent(firstName, lastname, dob, professorId);
				}
			}catch(Exception e)
			{
				Console.WriteLine(e.Message);
			}
			
			
		}
		private static void CreateNewStudent(string firstname, string lastname, DateTime dob, int professorId)
		{
			using(PostGrad postGrad = new PostGrad())
			{
				Student newStudent = new Student() { FirstName = firstname, LastName = lastname, DOB = dob, ProfessorId = professorId };
				postGrad.Student.Add(newStudent);
				Console.WriteLine(newStudent.FirstName + ", " + newStudent.LastName + ", " + newStudent.DOB + " saved");
				postGrad.SaveChanges();
			}

		}

		private static void DeleteStudentFrom()
        {
			using(PostGrad postGrad = new PostGrad())
            {
				List<Student> listOfStudents = postGrad.Student.ToList();

				foreach(Student students in listOfStudents)
                {
                    Console.WriteLine("id: " + students.Id + ", name" + students.FirstName + " " + students.LastName);
                }

				Console.WriteLine("Id of the student you want to delete: ");
				int idToStudent = Convert.ToInt32(Console.ReadLine());

				DeleteStudent(idToStudent);
			}
		}

		//method that delets student. Not called
		private static void DeleteStudent(int studentId)
        {
			using (PostGrad postGrad = new PostGrad())
            {
				Student studentToDelete = postGrad.Student.Find(studentId);
				postGrad.Remove(studentToDelete);

				postGrad.SaveChanges();
            }
        }

	}
}
