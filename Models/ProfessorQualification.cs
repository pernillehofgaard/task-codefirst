﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Task12_CodeFirst;

namespace Task11_CodeFirst.Models
{
    class ProfessorQualification
    {
        //Linking table
        public int ProfessorId { get; set; } //PK, FK
        public Professor Professor { get; set; }//PK, FK
        public int QualificationId { get; set; }
        public Qualification Qualification { get; set; }
        
    }
}
