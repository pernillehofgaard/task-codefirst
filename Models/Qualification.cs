﻿using System;
using System.Collections.Generic;
using System.Text;
using Task12_CodeFirst;

namespace Task11_CodeFirst.Models
{
	class Qualification
	{
        public int Id { get; set; } //PK
        public DateTime AwardedDate { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public ICollection<ProfessorQualification> ProfessorQualifications { get; set; }
    }
}
