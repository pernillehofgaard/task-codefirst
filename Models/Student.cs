﻿using System;
using System.Diagnostics;
using Task11_CodeFirst.Models;

namespace Task12_CodeFirst
{
    class Student
    {
        public int Id { get; set; } //PK
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }

        //Navigation property:
        public int ProfessorId { get; set; } //FK
        public Professor professor { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }

        public void skriv()
        {
            Console.WriteLine(Id + ", " + FirstName);
        }
    }

    






}
