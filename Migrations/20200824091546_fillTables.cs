﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Task12_CodeFirst.Migrations
{
    public partial class fillTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Professor",
                columns: new[] { "Id", "FirstName", "LastName", "Subject" },
                values: new object[,]
                {
                    { 1, "Kari", "Nordmann", "Coding" },
                    { 2, "Ola", "Nordmann", "Math" },
                    { 3, "Nick", "Lennox", "Coding" },
                    { 4, "Bjørn", "Blikstad", "Biology" },
                    { 5, "Dewald", "Els", "UI-design" }
                });

            migrationBuilder.InsertData(
                table: "ProfessorQualification",
                columns: new[] { "ProfessorId", "QualificationId" },
                values: new object[,]
                {
                    { 1, 5 },
                    { 1, 4 },
                    { 2, 3 },
                    { 3, 4 },
                    { 4, 1 },
                    { 5, 5 }
                });

            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "DOB", "FirstName", "LastName", "ProfessorId" },
                values: new object[,]
                {
                    { 5, new DateTime(1997, 10, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jon", "Otessen", 1 },
                    { 4, new DateTime(1985, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Marte", "Brekke", 2 },
                    { 3, new DateTime(1994, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Emil", "Kalstø", 3 },
                    { 2, new DateTime(1997, 2, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Oda", "Hofgaard", 4 },
                    { 1, new DateTime(1997, 2, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Pernille", "Hofgaard", 5 }
                });

            migrationBuilder.InsertData(
                table: "ResearchProject",
                columns: new[] { "Id", "StartDate", "StudentId", "Title" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "Corona Vaksine" },
                    { 2, new DateTime(1986, 4, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, "Radiation testing" },
                    { 3, new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Baby boom experiment" },
                    { 4, new DateTime(1940, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "Twin experiment" },
                    { 5, new DateTime(2012, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "End of the world" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 1, 4 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 1, 5 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 3, 4 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 4, 1 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 5, 5 });

            migrationBuilder.DeleteData(
                table: "ResearchProject",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ResearchProject",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ResearchProject",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ResearchProject",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ResearchProject",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
