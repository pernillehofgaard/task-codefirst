﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Task12_CodeFirst.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.InsertData(
                table: "Qualifications",
                columns: new[] { "Id", "AwardedDate", "Category", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(1997, 2, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Biology", "Bachelor" },
                    { 2, new DateTime(2004, 12, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Math", "PhD" },
                    { 3, new DateTime(1994, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Cooking", "Masters" },
                    { 4, new DateTime(1963, 4, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Coding", "Bachelor" },
                    { 5, new DateTime(1969, 1, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "UI-design", "Diploma" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Qualifications",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Qualifications",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Qualifications",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Qualifications",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Qualifications",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "DOB", "FirstName", "LastName", "ProfessorId" },
                values: new object[,]
                {
                    { 1, new DateTime(1997, 2, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Pernille", "Hofgaard", 5 },
                    { 2, new DateTime(1997, 2, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Oda", "Hofgaard", 4 },
                    { 3, new DateTime(1994, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Emil", "Kalstø", 3 },
                    { 4, new DateTime(1985, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Marte", "Brekke", 2 },
                    { 5, new DateTime(1997, 10, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jon", "Otessen", 1 }
                });
        }
    }
}
