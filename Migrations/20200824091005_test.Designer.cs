﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Task12_CodeFirst;

namespace Task12_CodeFirst.Migrations
{
    [DbContext(typeof(PostGrad))]
    [Migration("20200824091005_test")]
    partial class test
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.7")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Task11_CodeFirst.Models.ProfessorQualification", b =>
                {
                    b.Property<int>("ProfessorId")
                        .HasColumnType("int");

                    b.Property<int>("QualificationId")
                        .HasColumnType("int");

                    b.HasKey("ProfessorId", "QualificationId");

                    b.HasIndex("QualificationId");

                    b.ToTable("ProfessorQualification");
                });

            modelBuilder.Entity("Task11_CodeFirst.Models.Qualification", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("AwardedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Category")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Qualifications");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AwardedDate = new DateTime(1997, 2, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Category = "Biology",
                            Name = "Bachelor"
                        },
                        new
                        {
                            Id = 2,
                            AwardedDate = new DateTime(2004, 12, 24, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Category = "Math",
                            Name = "PhD"
                        },
                        new
                        {
                            Id = 3,
                            AwardedDate = new DateTime(1994, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Category = "Cooking",
                            Name = "Masters"
                        },
                        new
                        {
                            Id = 4,
                            AwardedDate = new DateTime(1963, 4, 25, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Category = "Coding",
                            Name = "Bachelor"
                        },
                        new
                        {
                            Id = 5,
                            AwardedDate = new DateTime(1969, 1, 31, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Category = "UI-design",
                            Name = "Diploma"
                        });
                });

            modelBuilder.Entity("Task11_CodeFirst.Models.ResearchProject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("StudentId")
                        .HasColumnType("int");

                    b.Property<string>("Title")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("StudentId");

                    b.ToTable("ResearchProject");
                });

            modelBuilder.Entity("Task12_CodeFirst.Professor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Subject")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Professor");
                });

            modelBuilder.Entity("Task12_CodeFirst.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DOB")
                        .HasColumnType("datetime2");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ProfessorId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ProfessorId");

                    b.ToTable("Student");
                });

            modelBuilder.Entity("Task11_CodeFirst.Models.ProfessorQualification", b =>
                {
                    b.HasOne("Task12_CodeFirst.Professor", "Professor")
                        .WithMany("professorQualifications")
                        .HasForeignKey("ProfessorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Task11_CodeFirst.Models.Qualification", "Qualification")
                        .WithMany("ProfessorQualifications")
                        .HasForeignKey("QualificationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Task11_CodeFirst.Models.ResearchProject", b =>
                {
                    b.HasOne("Task12_CodeFirst.Student", "Student")
                        .WithMany()
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Task12_CodeFirst.Student", b =>
                {
                    b.HasOne("Task12_CodeFirst.Professor", "professor")
                        .WithMany("Students")
                        .HasForeignKey("ProfessorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
