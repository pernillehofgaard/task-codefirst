﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Task12_CodeFirst.Migrations
{
    public partial class SeedDataTest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 1, 4 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 1, 5 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 3, 2 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualification",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 4, 1 });

            migrationBuilder.DeleteData(
                table: "ResearchProject",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ResearchProject",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ResearchProject",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ResearchProject",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ResearchProject",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Qualifications",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Qualifications",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Qualifications",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Qualifications",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Qualifications",
                keyColumn: "Id",
                keyValue: 5);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Professor",
                columns: new[] { "Id", "FirstName", "LastName", "Subject" },
                values: new object[,]
                {
                    { 11, "Kari", "Nordmann", "Coding" },
                    { 12, "Ola", "Nordmann", "Math" },
                    { 13, "Nick", "Lennox", "Coding" },
                    { 14, "Bjørn", "Blikstad", "Biology" },
                    { 15, "Dewald", "Els", "UI-design" }
                });

            migrationBuilder.InsertData(
                table: "Qualifications",
                columns: new[] { "Id", "AwardedDate", "Category", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(1997, 2, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Biology", "Bachelor" },
                    { 2, new DateTime(2004, 12, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Math", "PhD" },
                    { 3, new DateTime(1994, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Cooking", "Masters" },
                    { 4, new DateTime(1963, 4, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Coding", "Bachelor" },
                    { 5, new DateTime(1969, 1, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "UI-design", "Diploma" }
                });

            migrationBuilder.InsertData(
                table: "ResearchProject",
                columns: new[] { "Id", "StartDate", "StudentId", "Title" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "Corona Vaksine" },
                    { 2, new DateTime(1986, 4, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, "Radiation testing" },
                    { 3, new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Baby boom experiment" },
                    { 4, new DateTime(1940, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "Twin experiment" },
                    { 5, new DateTime(2012, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "End of the world" }
                });

            migrationBuilder.InsertData(
                table: "ProfessorQualification",
                columns: new[] { "ProfessorId", "QualificationId" },
                values: new object[,]
                {
                    { 4, 1 },
                    { 3, 2 },
                    { 2, 3 },
                    { 1, 4 },
                    { 1, 5 }
                });
        }
    }
}
